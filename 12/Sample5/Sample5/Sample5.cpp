...

//buy함수 선언
void buy(Car& c);

int main()
{
    Car car1;
    
    car1.setNumGas(1234, 20.5);
    
    buy(car1);
    
    return 0;
}

//buy함수의 정의
void buy(Car& c)
{
    int n = c.getNum();
    double g = c.getGas();
    cout << "차량 번호 " << n << " 연료의 양 " << g << "인 자동차를 구입했습니다.\n";
}